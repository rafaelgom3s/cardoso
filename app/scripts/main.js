(function() {
  'use strict';

  var cardoso = angular
    .module('cardoso', ['ngRoute', 'ngMaterial']);

  cardoso.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'templates/home.html',
        login: true
      })
      .when('/profile', {
        templateUrl: 'templates/profile.html'
      })
      .when('/work', {
        templateUrl: 'templates/work.html'
      })
      .when('/work/:id', {
        templateUrl: 'templates/project.html',
        controller: 'ProjectController'
      })
      .otherwise({
        redirectTo: '/'
      });

    $locationProvider.html5Mode(false);
  }]);

  cardoso.controller('MainController', ['$scope', '$mdMedia', '$location', 'Profile', 'Projects', function ($scope, $mdMedia, $location, Profile, Projects) {
    $scope.projects = Projects;
    $scope.timeline = Profile;

    $scope.project = {};

    $scope.isHomepage = function() {
      var url = $location.url();
      return _.isEmpty(url) || url === '/';
    };

    $scope.isProfile = function() {
      return $location.url() == '/profile';
    };

    $scope.isWork = function() {
      return $location.url() == '/work';
    };

    $scope.isProject = function() {
      return _.contains($location.url(), '/work');
    };

    $scope.showProject = function (project) {
      $scope.project = project;
      $location.url('/work/' + project.id);
    };

    $scope.addLeftMargin = $mdMedia('gt-sm');
    $scope.$watch(function() {
      return $mdMedia('gt-sm');
    }, function(addMargin) {
      $scope.addLeftMargin = addMargin;
    });

    $scope.addRightMargin = $mdMedia('gt-sm');
    $scope.$watch(function() {
      return $mdMedia('gt-sm');
    }, function(addMargin) {
      $scope.addRightMargin = addMargin;
    });
  }]);

})();
