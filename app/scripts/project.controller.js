(function() {
  'use strict';

  angular.module('cardoso')
    .controller('ProjectController', ['$scope', '$routeParams', 'Projects', ProjectController]);

  function ProjectController($scope, $routeParams, Projects) {
    $scope.project = _.find(Projects, {'id': parseInt($routeParams.id)}) || {};

    $scope.hasPreviousProject = function() {
      return _.isNumber($scope.project.id) && $scope.project.id > 1;
    };
    $scope.hasNextProject = function() {
      return _.isNumber($scope.project.id) && Projects.length > $scope.project.id;
    };

    $scope.getPrevUrl = function () {
      return '#/work/' + ($scope.project.id - 1);
    };
    $scope.getNextUrl = function () {
      return '#/work/' + ($scope.project.id + 1);
    };

    $scope.getTags = function () {
      return $scope.project.tags.join(', ') || null;
    };

    $scope.getDesigners = function () {
      return $scope.project.designers.join(', ') || null;
    };
  }
})();
