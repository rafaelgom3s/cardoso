(function() {
  'use strict';

  angular.module('cardoso')
    .constant('Projects', [
      {
        'id': 1,
        'title': 'Google',
        'url': {
          'value': 'https://get.google.com/tango/',
          'type': 'www'
        },
        'description': 'Was part of the Google\'s lead UX team that was exploring all sorts of apps and experiences using Tango Depth sensing technology. (can\'t share more images, as they are still under development).',
        'learned': 'That the gap between humans and technology is becoming smaller. Was very happy to work with the same technology that I was experimenting with 8 years before that now runs on a smartphone and not on a huge PC tower.',
        'tags': [
          'RESEARCH',
          'CONCEPT',
          'UX AND UI DESIGN'
        ],
        'year': '2016',
        'location': 'Mountain View, USA',
        'cover': '/images/tango/tango_3.jpg',
        'media': [
          {'url': '/images/tango/tango_1.jpg'},
          {'url': '/images/tango/tango_2.jpg'},
          {'url': '/images/tango/tango_3.jpg'},
          {'url': '/images/tango/tango_4.jpg'},
          {'url': '/images/tango/tango_5.jpg'}
        ]
      },
      {
        'id': 2,
        'title': 'ADP',
        'description': 'Design and developed a platform that aggregates several sub-systems used by Portugal\'s Water Company engineers. To bring consistency to a widely scattered data base.',
        'learned': 'Close groups tend to create their own patterns, ways to communicate and structures which then need to interact with other groups with different set of tools. I learned the common patterns amongst these groups to design the platform. (still under construction)',
        'tags': [
          'RESEARCH',
          'CONCEPT AND UI DESIGN'
        ],
        'year': '2016',
        'location': 'Lisbon, Portugal',
        'cover': '/images/epal/enki_1.jpg',
        'media': [
          {'url': '/images/epal/enki_1.jpg'},
          {'url': '/images/epal/enki_2.jpg'},
          {'url': '/images/epal/enki_3.jpg'},
          {'url': '/images/epal/enki_4.jpg'},
          {'url': '/images/epal/enki_5.jpg'}
        ]
      },
      {
        'id': 3,
        'title': 'lytro',
        'url': {
          'value':'http://www.lytro.com',
          'type': 'www'
        },
        'description': 'Explorations on user experience and interactions for a new digital camera from Lytro that has a proprietary disruptive technology called "light field photography".',
        'learned': 'Great teams can achieve great results. I was lucky to be integrated with an amazing design team.',
        'tags': [
          'Concept',
          'UX Design'
        ],
        'year': 2015,
        'location': 'Montain View, USA',
        'cover': '/images/covers/lytro_camera_cover.jpg',
        'media': [
          {'url': '/images/lytro_camera/Drop-in-Screen_72dpi_BW.jpg'},
          {'url': '/images/lytro_camera/IMG_0012.jpg'},
          {'url': '/images/lytro_camera/Lytro-Illum-3.jpg'},
          {'url': '/images/lytro_camera/lytro-illum-dial.jpg'},
          {'url': '/images/lytro_camera/lytro_wires.jpg'},
          {'url': '/images/lytro_camera/right1.jpg'}
        ]
      },
      {
        'id': 4,
        'title': 'lytro',
        'url': {
          'value':'http://www.lytro.com',
          'type': 'www'
        },
        'description': 'Proposal of a timeline based product management application. The challenge was how can we make it more visual and appealing for non technical stakeholders.',
        'learned': 'A lot about project/code/product management tools. That things can be done differently.',
        'tags': [
          'Concept',
          'UX Design'
        ],
        'year': 2014,
        'location': 'Montain View, USA',
        'cover': '/images/covers/lytro_timeline_cover.jpg',
        'media': [
          {'url': '/images/lytro_timeline/L_timeline.jpg'}
        ]
      },
      {
        'id': 5,
        'title': 'impossible',
        'url': {
          'value':'http://www.impossible.com',
          'type': 'www'
        },
        'description': 'Proposal of a watchface for the social sharing platform, impossible.',
        'learned': 'That a watch screen is too small.',
        'tags': [
          'Concept',
          'Visual Design'
        ],
        'year': 2014,
        'location': 'London, United Kingdom',
        'cover': '/images/covers/apple_watch_1.jpg',
        'media': [
          {'url': '/images/apple_watch_1.jpg'},
          {'url': '/images/apple_watch_2.jpg'},
          {'url': '/images/apple_watch_3.jpg'}
        ]
      },
    {
      'id': 6,
      'title': 'Samsung Market',
      'url': {
        'value': 'https://storage.googleapis.com/cardoso-portfolio/video/S_market_animation_1_v2.mp4',
        'type': 'video'
      },
      'description': 'Samsung wanted to create a market application that could aggregate all their soft products.',
      'learned': 'That it\'s still possible to explore the boundaries of user experience and interactions.',
      'tags': [
        'Concept',
        'UX and UI Design'
      ],
      'year': 2013,
      'location': 'Seoul, South Korea',
      'cover': '/images/covers/s_market_0.jpg',
      'media': [
        {
          'url': '/images/s_market_0.jpg',
          'type': 'image'
        },
        {
          'url': '/images/s_market_1.jpg',
          'type': 'image'
        },
        {
          'url': '/images/s_market_2.jpg',
          'type': 'image'
        },
        {
          'url': '/images/s_market_3.jpg',
          'type': 'image'
        },
        {
          'url': '/images/s_market_4.jpg',
          'type': 'image'
        }
      ]
    },
    {
      'id': 7,
      'title': 'Samsung',
      'description': 'Several studies for a home application for Samsung tablets.',
      'learned': 'A big client does not always know what it wants. The relation with other nationalities is very enriching.',
      'tags': [
        'UX and UI Design'
      ],
      'year': 2014,
      'location': 'San Francisco, USA',
      'cover': '/images/covers/s_always_tablet_3.jpg',
      'media': [
        {'url': '/images/s_always_tablet_1.jpg'},
        {'url': '/images/s_always_tablet_2.jpg'},
        {'url': '/images/s_always_tablet_3.jpg'}
      ]
    },
    {
      'id': 8,
      'title': 'Fairphone V1',
      'url': {
        'value': 'http://www.fairphone.com',
        'type': 'www'
      },
      'description': 'Mobile phone with a conscience. First UI studies for the first version of the phone.',
      'learned': 'That it feels great to work on a project with a strong social concern, that it\'s not about doing just another phone. So much Android "dos" and "dont\'s."',
      'tags': [
        'Concept',
        'UX and UI Design'
      ],
      'year': 2013,
      'location': 'Amsterdam, Netherlands',
      'cover': '/images/covers/fairphone_promo_photo_7.jpg',
      'media': [
        {'url': '/images/fairphone_download.jpg'},
        {'url': '/images/fairphone_FP_UI_1.jpg'},
        {'url': '/images/fairphone_FP_UI_2.jpg'},
        {'url': '/images/fairphone_promo_photo_5.jpg'},
        {'url': '/images/fairphone_promo_photo_6.jpg'},
        {'url': '/images/fairphone_promo_photo_7.jpg'}
      ]
    },
    {
      'id': 9,
      'title': 'Orona Showroom',
      'description': 'Proposal for a big elevator constructor. The challenge: how can we engage with our clients in a new and surprising way that reflects the innovation of our products. ',
      'learned': 'A lot about elevators. How to sell innovation without the real product. How to sell the future.',
      'tags': [
        'Concept',
        'Space Design',
        'Art direction'
      ],
      'year': 2013,
      'designers': [
        'Luis Mestre',
        'Luis Vargas'
      ],
      'location': 'San Sebastián, Spain',
      'cover': '/images/covers/ORONA3.1070.jpg',
      'media': [
        {'url': '/images/orona/ORONA3.1067.jpg'},
        {'url': '/images/orona/ORONA3.1069.jpg'},
        {'url': '/images/orona/ORONA3.1070.jpg'},
        {'url': '/images/orona/ORONA3.1072.jpg'},
        {'url': '/images/orona/ORONA3.1074.jpg'},
        {'url': '/images/orona/ORONA3.1076.jpg'},
        {'url': '/images/orona/ORONA3.1077.jpg'},
        {'url': '/images/orona/ORONA3.1078.jpg'},
        {'url': '/images/orona/ORONA3.1085.jpg'},
        {'url': '/images/orona/ORONA3.1099.jpg'}
      ]
    },
    {
      'id': 10,
      'title': 'Brain surgery application',
      'url': {
        'value': 'http://www.youtube.com/watch?v=91F6zErnCrs',
        'type': 'video'
      },
      'description': 'Gesture based application to be used during brain surgery.',
      'learned': 'A lot about a surgeon\'s work flow and restrictions. Amazing opportunity to play with new interactions with constant validations with the actual users, surgeons.',
      'tags': [
        'Art direction',
        'UI Design'
      ],
      'year': 2012,
      'designers': [
        'Dania Afonso'
      ],
      'location': 'Lisbon, Portugal',
      'cover': '/images/covers/yscope_TV_screen.jpg',
      'media': [
        {'url': '/images/yscope_exames_1.jpg'},
        {'url': '/images/yscope_exames_2.jpg'},
        {'url': '/images/yscope_medidas_dim.jpg'},
        {'url': '/images/yscope_medidas_quatro.jpg'},
        {'url': '/images/yscope_seis_imagens.jpg'},
        {'url': '/images/yscope_TV_screen.jpg'}
      ]
    },
    {
      'id': 11,
      'title': 'Lisboa story centre',
      'url': {
        'value': 'http://www.ydreams.com/#!/en/case/6',
        'type': 'www'
      },
      'description': 'Museum about Lisbon and its historic earthquake. We were responsible for a specific room about Lisbon\'s main events.',
      'learned': 'Projection mapping and how to make a huge pepper\'s ghost (pure magic).',
      'tags': [
        'Concept',
        'Space design',
        'Art direction',
        'UX Design'
      ],
      'year': 2012,
      'designers': [
        'Daniela Pretorius',
        'Luis Mestre',
        'Luis Vargas'
      ],
      'location': 'Lisbon, Portugal',
      'cover': '/images/covers/14_LSC_views.jpg',
      'media': [
        {'url': '/images/LSC/9_LSC_views.jpg'},
        {'url': '/images/LSC/11_LSC_views.jpg'},
        {'url': '/images/LSC/12_LSC_views.jpg'},
        {'url': '/images/LSC/14_LSC_views.jpg'},
        {'url': '/images/LSC/16_LSC_views.jpg'},
        {'url': '/images/LSC/18_LSC_views.jpg'},
        {'url': '/images/LSC/20_LSC_views.jpg'},
        {'url': '/images/LSC/21_LSC_views.jpg'}
      ]
    },
    {
      'id': 12,
      'title': 'Phone house concept store',
      'description': 'How could we make a whole store with digital applications that could enhance the user\'s experience in a relevant way.',
      'learned': 'We learned how to develop a complete concept store, but it was never fully developed. We learned that PT phone companies don\'t keep their promises.',
      'tags': [
        'Concept',
        'Art direction'
      ],
      'year': 2012,
      'designers': [
        'Luis Mestre',
        'Luis Vargas'
      ],
      'location': 'Lisbon, Portugal',
      'cover': '/images/covers/PT2.129.jpg',
      'media': [
        {'url': '/images/PT/PT2.128.jpg'},
        {'url': '/images/PT/PT2.129.jpg'},
        {'url': '/images/PT/PT2.130.jpg'},
        {'url': '/images/PT/PT2.131.jpg'},
        {'url': '/images/PT/PT2.135.jpg'},
        {'url': '/images/PT/PT2.136.jpg'},
        {'url': '/images/PT/PT2.137.jpg'},
        {'url': '/images/PT/PT2.141.jpg'}
      ]
    },
    {
      'id': 13,
      'title': 'Quartz museum',
      'url': {
        'value': 'http://www.youtube.com/watch?v=QVsU7QswuzI',
        'type': 'video'
      },
      'description': 'The only museum in the world dedicated to quartz (or so they say).',
      'learned': 'Everything there is to know about this mineral. That some groundbreaking interactions are not the most suited for all users. But felt damn good to think about them.',
      'tags': [
        'Concept',
        'Art direction',
        'UX Design'
      ],
      'year': 2012,
      'designers': [
        'Ana Carvalho',
        'Luis Mestre',
        'Katja Hearing'
      ],
      'location': 'Viseu, Portugal',
      'cover': '/images/covers/69_museu_quartzo.jpg',
      'media': [
        {'url': '/images/quartzo/34_museu_quartzo.jpg'},
        {'url': '/images/quartzo/43_museu_quartzo.jpg'},
        {'url': '/images/quartzo/45_museu_quartzo.jpg'},
        {'url': '/images/quartzo/54_museu_quartzo.jpg'},
        {'url': '/images/quartzo/68_museu_quartzo.jpg'},
        {'url': '/images/quartzo/69_museu_quartzo.jpg'},
        {'url': '/images/quartzo/71_museu_quartzo.jpg'},
        {'url': '/images/quartzo/76_museu_quartzo.jpg'},
        {'url': '/images/quartzo/ciclo2.jpg'}
      ]
    },
    {
      'id': 14,
      'title': 'interactive virtual world',
      'url': {
        'value': 'http://www.youtube.com/watch?v=gaWbE4iUWqk',
        'type': 'video'
      },
      'description': 'A playful way to engage with kids in a commercial space.',
      'learned': 'Kids are amazing at breaking the digital barriers. They\'re so easy as users.',
      'tags': [
        'Concept',
        'Art direction',
        'UX Design'
      ],
      'year': 2012,
      'designers': [
        'Luis Mestre',
        'Luis Vargas'
      ],
      'location': 'Istanbul, Turkey',
      'cover': '/images/covers/DSCF0181.jpg',
      'media': [
        {'url': '/images/virtual_world/DSCF0181.jpg'},
        {'url': '/images/virtual_world/DSCF0217-2.jpg'},
        {'url': '/images/virtual_world/kiosk_pet_edit_colors.jpg'},
        {'url': '/images/virtual_world/kiosk_screensaver_4.jpg'},
        {'url': '/images/virtual_world/world.jpg'}
      ]
    },
    {
      'id': 15,
      'title': 'interactive virtual aquarium',
      'url': {
        'value': 'http://www.youtube.com/watch?v=gaWbE4iUWqk',
        'type': 'video'
      },
      'description': 'A playful way to engage with kids in a commercial space.',
      'learned': 'Kids are amazing at breaking the digital barriers. They\'re so easy as users.',
      'tags': [
        'Concept',
        'Art direction',
        'UX Design'
      ],
      'year': 2011,
      'designers': [
        'Luis Mestre',
        'Luis Vargas'
      ],
      'location': 'Oeiras, Portugal',
      'cover': '/images/covers/25_fixelandia.jpg',
      'media': [
        {'url': '/images/fixelandia/02_fixelandia.jpg'},
        {'url': '/images/fixelandia/03_fixelandia.jpg'},
        {'url': '/images/fixelandia/14_fixelandia.jpg'},
        {'url': '/images/fixelandia/17_fixelandia.jpg'},
        {'url': '/images/fixelandia/19_fixelandia.jpg'},
        {'url': '/images/fixelandia/21_fixelandia.jpg'},
        {'url': '/images/fixelandia/22_fixelandia.jpg'},
        {'url': '/images/fixelandia/24_fixelandia.jpg'},
        {'url': '/images/fixelandia/25_fixelandia.jpg'},
        {'url': '/images/fixelandia/26_fixelandia.jpg'},
        {'url': '/images/fixelandia/25_fixelandia.jpg'},
        {'url': '/images/fixelandia/aquario_lego_1.jpg'},
        {'url': '/images/fixelandia/aquario_lego_2.jpg'}
      ]
    },
    {
      'id': 16,
      'title': 'Santander visitors center',
      'url': {
        'value': 'http://www.youtube.com/watch?v=bzDIJ6TTc6w',
        'type': 'video'
      },
      'description': 'Show off visitors center for the biggest Spanish bank.',
      'learned': 'Creativity has no limits when money flows. It was incredible to be able to create a set of robots and think about their behavior.',
      'tags': [
        'Concept',
        'UI Design'
      ],
      'year': 2009,
      'designers': [
        'Leonel Duarte',
        'Luis Vargas'
      ],
      'location': 'Madrid, Spain',
      'cover': '/images/covers/31_maquete.jpg',
      'media': [
        {'url': '/images/santander/21_robots.jpg'},
        {'url': '/images/santander/28_maquete.jpg'},
        {'url': '/images/santander/31_maquete.jpg'},
        {'url': '/images/santander/32_maquete.jpg'},
        {'url': '/images/santander/34_parede.jpg'},
        {'url': '/images/santander/35_parede.jpg'},
        {'url': '/images/santander/37_parede.jpg'},
        {'url': '/images/santander/38_parede.jpg'}
      ]
    },
    {
      'id': 17,
      'title': 'Nature interpretation center',
      'url': {
        'value': 'http://www.youtube.com/watch?v=mZRkp2NWbc0',
        'type': 'video'
      },
      'description': 'River Tejo Natural Park Interpretation Center.',
      'learned': 'A lot of things about nature. Visitors behavior, mainly kids. Unexpected interactive solutions can have great impact on the retention of information.',
      'tags': [
        'Concept',
        'Art direction'
      ],
      'year': 2009,
      'designers': [
        'Luis Mestre',
        'Pedro Rodrigues'
      ],
      'location': 'Castelo Branco, Portugal',
      'cover': '/images/covers/26_PNTI_piso0.jpg',
      'media': [
        {'url': '/images/nature_center/03_PNTI_cave.jpg'},
        {'url': '/images/nature_center/06_PNTI_raioX.jpg'},
        {'url': '/images/nature_center/26_PNTI_piso0.jpg'},
        {'url': '/images/nature_center/27_PNTI_falesia.jpg'},
        {'url': '/images/nature_center/31_PNTI_piso0.jpg'},
        {'url': '/images/nature_center/33_PNTI_percursos.jpg'},
        {'url': '/images/nature_center/3.jpg'}
      ]
    },
    {
      'id': 18,
      'title': 'digital signage',
      'description': 'Proposal for Lisbon municipality.',
      'learned': 'To look for the right materials. To work with the right partners and listen to what they have to teach you. Concept ahead of its time, technology and user experience wasn\'t quite there.',
      'tags': [
        'Concept',
        'Art direction'
      ],
      'year': 2008,
      'designers': [
        'Dania Afonso'
      ],
      'location': 'Lisbon, Portugal',
      'cover': '/images/covers/ds_azulejos2_final.jpg',
      'media': [
        {'url': '/images/ds_azulejos1_final.jpg'},
        {'url': '/images/ds_azulejos5_final.jpg'},
        {'url': '/images/ds_mupi1_final.jpg'},
        {'url': '/images/ds_mupi7distorted_final.jpg'}
      ]
    },
    {
      'id': 19,
      'title': 'Graphic design',
      'url': {
        'value': 'http://www.formasdopossivel.com',
        'type': 'www'
      },
      'description': 'A long long time ago. I did all things related with graphic design. My biggest passions were layout, books and magazines.',
      'learned': 'Everything. Design production, how to run a company, invoicing, to be creative, to be humble, to learn what truly means to be a design professional. I loved the smell of fresh printed paper.',
      'tags': [
        'Concept',
        'Graphic design'
      ],
      'year': '1998/2006',
      'location': 'Lisbon, Portugal',
      'cover': '/images/covers/1270740024850.jpg',
      'media': [
        {'url': '/images/graphic_design/1267724494496.jpg'},
        {'url': '/images/graphic_design/1270649269910.jpg'},
        {'url': '/images/graphic_design/1270724880465.jpg'},
        {'url': '/images/graphic_design/1270740024850.jpg'},
        {'url': '/images/graphic_design/1270805353081.jpg'},
        {'url': '/images/graphic_design/1270809319987.jpg'},
        {'url': '/images/graphic_design/1270809383826.jpg'},
        {'url': '/images/graphic_design/1270811533368.jpg'},
        {'url': '/images/graphic_design/1270815633673.jpg'},
        {'url': '/images/graphic_design/1270815651265.jpg'}
      ]
    }]);
})();
