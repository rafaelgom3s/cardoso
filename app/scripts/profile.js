(function() {
  'use strict';

  angular.module('cardoso')
    .constant('Profile', [{
      'year': '1973',
      'title': 'CAME INTO THIS WORLD',
      'description': 'Born in Lisbon. Within 3 days moved to Almada, Lisbon\'s south bank.',
      'learned': 'To play Lego with my brother and the harsh lessons of life.'
    }, {
      'year': '1989',
      'title': 'PHOTOGRAPHY',
      'description': 'Bought a Nikon F801 and a couple of lenses, after a summer job.',
      'learned': 'That photography can become an addiction for the rest of your life.',
      'url': {
        'value': 'https://pedronc.smugmug.com/Personal',
        'type': 'WWW'
      }
    }, {
      'year': '1992 - 1997',
      'title': 'BASIC STUDIES',
      'description': 'Accepted into the oldest design school in the country to study Communication Design. Faculdade de Belas Artes de Lisboa.',
      'learned': 'The biggest lesson was that I wanted to be a designer for as long I live. Oh, and I learned that with friends.',
      'url': {
        'value': 'http://www.belasartes.ulisboa.pt',
        'type': 'WWW'
      }
    }, {
      'year': '1994 - ...',
      'title': 'MOUNTAIN BIKE',
      'description': 'Again I discovered another addiction. Mountains and all things related, being a bike the best way to get there.',
      'learned': 'I learned meaning and purpose. To set goals. To overcome your limits (and they can stretch a lot). To respect our birth place, Nature.'
    }, {
      'year': '1998 - ...',
      'title': 'MY OWN COMPANY',
      'description': 'I thought I was as good as anyone else, so I opened my own design company with a couple of friends. Atelier Formas do Possível.',
      'learned': 'Man, I had no idea what I was getting into! Would I do it all over again the same way? Definitely. The best thing, it still exists.',
      'url': {
        'value': 'http://www.formasdopossivel.com',
        'type': 'WWW'
      }
    }, {
      'year': '2000 - 2005',
      'title': 'TEACHING',
      'description': 'Someone thought that I had something to share with younger people. I became a university teacher.',
      'learned': 'Teaching can be addictive. I learned as much as I taught. Some students became friends, some company partners, others still write me.',
      'url': {
        'value': 'http://estal.pt',
        'type': 'WWW'
      }
    }, {
      'year': '2003 - ...',
      'title': 'FATHER',
      'description': 'Started one of my life projects. Made a daughter called Lua.',
      'learned': 'To be a kid again. Not to be afraid to fail and make mistakes. True commitment.'
    }, {
      'year': '2006 - 2013',
      'title': 'ART DIRECTOR',
      'description': 'Was invited to create a design team at a tech/creative/new interactions company, that had just got 8M to create amazing stuff. YDREAMS.',
      'learned': 'Creativity has no limits. And this applies literally to everyone, not only designers. And I learned all that there is to learn about team/project management, team building, random thoughts... the list is huge.',
      'url': {
        'value': 'http://www.ydreams.com',
        'type': 'WWW'
      }
    }, {
      'year': '2006 - ...',
      'title': 'FATHER AGAIN',
      'description': 'Made another life project, this time we called him Simão.',
      'learned': 'More or less multiplied by two the fun of the previous.'
    }, {
      'year': '2013 - 2015',
      'title': 'SENIOR UX/UI DESIGNER',
      'description': 'At Kwamecorp I was challenged to give life to ideas and problems from top tier "Silicon Valley" clients.',
      'learned': 'It\'s amazing to work with other nationalities. Man, you learn so much. In the big tech world, they truly believe in what they\'re doing.',
      'url': {
        'value': 'https://www.kwamecorp.com',
        'type': 'WWW'
      }
    }, {
      'year': '2016 - ...',
      'title': 'FREELANCE',
      'description': 'To experience the non existing freedom of working for me. Looking for That project and looking for That client to deliver That experience.',
      'learned': 'I need people to work with. Trust is a wonderful thing and I need to conquer that trust. I\'m pushed to learn more tools and processes.'
    }, {
      'year': '2016 - ...',
      'title': 'Senior Product Designer',
      'description': 'Streetbees is full-on startup life. There\'s no comfort zone. I can push all my learnings towards one single direction. I can finally focus. I can help create a product.',
      'learned': 'To live and cycle in London. You never know what you need. Nothing is ever defined. Whatever tools helps you make it faster.',
      'url': {
       'value': 'http://www.streetbees.com',
       'type': 'WWW'
      }
    }]);
})();
