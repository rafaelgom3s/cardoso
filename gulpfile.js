var gulp = require('gulp');
var gulpLoadPlugins = require( 'gulp-load-plugins');
var browserSync = require('browser-sync');
var del = require('del');
var wiredep = require( 'wiredep');
var useref = require('gulp-useref');
var minifyHtml = require('gulp-minify-html');
var minifyCss = require('gulp-minify-css');
var imagemin = require('gulp-imagemin');
var gulpif = require('gulp-if');
var uglify = require('gulp-uglify');
var pump = require('pump');

var $ = gulpLoadPlugins();
var reload = browserSync.reload;

gulp.task('styles', function() {
  return gulp.src('app/styles/*.{scss,sass}')
    .pipe($.plumber())
    .pipe($.sourcemaps.init())
    .pipe($.sass.sync({
      outputStyle: 'expanded',
      precision: 10,
      includePaths: ['.']
    }).on('error', $.sass.logError))
    .pipe($.autoprefixer({browsers: ['last 1 version']}))
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest('.tmp/styles'))
    .pipe(reload({stream: true}));
});

function lint(files, options) {
  return function() {
    return gulp.src(files)
      .pipe(reload({stream: true, once: true}))
      .pipe($.eslint(options))
      .pipe($.eslint.format())
      .pipe($.if(!browserSync.active, $.eslint.failAfterError()));
  };
}

var testLintOptions = {
  env: {
    mocha: false
  }
};

gulp.task('lint', lint('app/scripts/**/*.js'));
gulp.task('lint:test', lint('test/spec/**/*.js', testLintOptions));

gulp.task('html', ['styles', 'templates'], function(cb) {
  pump([
      gulp.src('app/*.html'),
      useref({searchPath: ['.tmp', 'app', '.']}),
      $.if('*.js', uglify()),
      $.if('*.css', minifyCss()),
      $.if('*.html', minifyHtml({conditionals: true, loose: true})),
      gulp.dest('dist/public')
    ],
    cb
  );
});

gulp.task('templates', ['styles'], function(cb) {
  pump([
    gulp.src('app/templates/*.html'),
    gulp.dest('dist/public/templates')
  ], cb);
});

gulp.task('images', function() {
  pump([
    gulp.src('app/images/**/*'),
    $.if($.if.isFile, $.cache($.imagemin({
      progressive: true,
      interlaced: true,
      // don't remove IDs = require( SVGs, they are often used
      // as hooks for embedding and styling
      svgoPlugins: [{cleanupIDs: false}]
    }))),
    gulp.dest('dist/public/images')
  ]);
});

gulp.task('fonts', function() {
  return gulp.src('app/fonts/**/*')
    .pipe(gulp.dest('.tmp/fonts'))
    .pipe(gulp.dest('dist/public/fonts'));
});

gulp.task('extras', function() {
  return gulp.src([
    'app/*.*',
    '!app/*.html'
  ], {
    dot: true
  }).pipe(gulp.dest('dist/public'));
});

gulp.task('clean', del.bind(null, ['.tmp', 'dist/public']));

gulp.task('serve', ['styles', 'fonts'], function() {
  browserSync({
    notify: false,
    port: 9000,
    server: {
      baseDir: ['.tmp', 'app']
    }
  });

  gulp.watch([
    'app/*.html',
    'app/templates/*.html',
    'app/scripts/**/*.js',
    'app/images/**/*',
    '.tmp/fonts/**/*'
  ]).on('change', reload);

  gulp.watch('app/styles/**/*.{scss,sass}', ['styles']);
  gulp.watch('app/fonts/**/*', ['fonts']);
});

gulp.task('serve:dist', function() {
  browserSync({
    notify: false,
    port: 9000,
    server: {
      baseDir: ['dist/public']
    }
  });
});

gulp.task('serve:test', function() {
  browserSync({
    notify: false,
    port: 9000,
    ui: false,
    server: {
      baseDir: 'test'
    }
  });

  gulp.watch('test/spec/**/*.js').on('change', reload);
  gulp.watch('test/spec/**/*.js', ['lint:test']);
});

// inject components
gulp.task('wiredep', function() {
  gulp.src('app/styles/*.scss')
    .pipe(wiredep({
      ignorePath: /^(\.\.\/)+/
    }))
    .pipe(gulp.dest('app/styles'));

  gulp.src('app/*.html')
    .pipe(wiredep({
      ignorePath: /^(\.\.\/)*\.\./
    }))
    .pipe(gulp.dest('app'));
});

gulp.task('build', ['html', 'images', 'fonts', 'extras'], function() {
  return gulp.src('dist/public/**/*').pipe($.size({title: 'build', gzip: true}));
});

gulp.task('default', ['clean'], function() {
  gulp.start('build');
});
